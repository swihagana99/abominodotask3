package Command;

import controller.*;
import view.IOLibrary;
public class CommandAradMain {
    public static void main(String args[]){
        Aardvark ardvark = new Aardvark();

        placeDomAardCommand placeDomAardComm = new placeDomAardCommand(ardvark);

        CommandAardInvoker aardvarkCommandInvoker = new CommandAardInvoker(placeDomAardComm);
        aardvarkCommandInvoker.forwardGame();

        System.out.println("Do you need to undo(Y : yes , N : no) :");
        String undo = IOLibrary.getString();
        if(undo.equalsIgnoreCase("Y")) {
        	aardvarkCommandInvoker.backwardGame();
        }
    }
}
