package Command;

public class CommandAardInvoker {
	 private CommandAradInterface placeDomino;

	    public CommandAardInvoker(placeDomAardCommand placeDomAardCommand){
	        this.placeDomino = placeDomAardCommand;
	    }

	    public void forwardGame(){
	        placeDomino.playGame();
	    }

	    public void backwardGame(){
	        placeDomino.undoGame();
	    }
		
}
