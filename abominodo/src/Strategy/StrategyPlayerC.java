package Strategy;

import controller.Aardvark;
public class StrategyPlayerC implements Strategy{
    @Override
    public void autoPlay() {
        Aardvark aardvark = new Aardvark();
        aardvark.playerName = "Player C";
        aardvark.play();
    }
}