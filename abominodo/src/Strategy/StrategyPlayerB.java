package Strategy;

import controller.Aardvark;
public class StrategyPlayerB implements Strategy{
    @Override
    public void autoPlay() {
        Aardvark aardvark = new Aardvark();
        aardvark.playerName = "Player B";
        aardvark.play();
    }
}