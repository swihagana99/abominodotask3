package Strategy;

import controller.Aardvark;
public class StrategyPlayerA implements Strategy{
    @Override
    public void autoPlay() {
        Aardvark aardvark = new Aardvark();
        aardvark.playerName = "Player A";
        aardvark.play();
    }
}
