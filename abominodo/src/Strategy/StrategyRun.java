package Strategy;
import controller.Aardvark;

public class StrategyRun {
    public static void main(String args[]){

        ContextStrategy contextStr = new ContextStrategy(new StrategyPlayerA());
        contextStr.executeStrategy();


        contextStr = new ContextStrategy(new StrategyPlayerB());
        contextStr.executeStrategy();

        contextStr = new ContextStrategy(new StrategyPlayerC());
        contextStr.executeStrategy();

    }
}
